<?php

namespace app\models;

use app\components\utils\AdvancedStringHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\caching\Dependency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string|null $username
 * @property string|null $password
 * @property string|null $authKey
 * @property string|null $accessToken
 * @property bool|null $status
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property SpotifyToken $activeSpotifyToken
 *
 * @property SpotifyToken[] $spotifyTokens
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username'], 'unique'],
            [['username', 'password'], 'required'],
            [['username', 'password', 'authKey', 'accessToken'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Password'),
            'authKey' => Yii::t('app', 'Auth Key'),
            'accessToken' => Yii::t('app', 'Access Token'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    public function getUserCacheKey(string $key): string
    {
        return AdvancedStringHelper::makeUpCacheKey([$this->id, $key]);
    }

    public function popFromUserCache(string $key)
    {
        $cacheValue = $this->getFromUserCache($key);
        $this->deleteFromUserCache($key);
        return $cacheValue;
    }

    public function deleteFromUserCache(string $key)
    {
        return Yii::$app->cache->delete(AdvancedStringHelper::makeUpCacheKey([$this->id, $key]));
    }

    public function getFromUserCache(string $key)
    {
        return Yii::$app->cache->get(AdvancedStringHelper::makeUpCacheKey([$this->id, $key]));
    }

    public function setToUserCache(string $key, $value, int $duration = null, Dependency $dependency = null)
    {
        return Yii::$app->cache->set(AdvancedStringHelper::makeUpCacheKey([$this->id, $key]), $value, $duration, $dependency);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return array|ActiveRecord|null
     */
    public static function findByUsername(string $username)
    {
        return static::find()->where(['username' => $username])->one();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword(string $password)
    {
        return $this->password === $password;
    }

    public function getActiveSpotifyToken(string $scope = null)
    {
        $cacheKey = $scope ? "spotify_token_$scope" : 'spotify_token';

        $spotifyToken = $this->getFromUserCache($cacheKey);

        if ($spotifyToken) {
            return $spotifyToken;
        }

        $spotifyToken = $this->getSpotifyTokens()
            ->filterWhere(['scope' => $scope])
            ->one();

        if ($spotifyToken) {
            $this->setToUserCache($cacheKey, $spotifyToken);
        }

        return $spotifyToken;
    }

    public function setActiveSpotifyToken(SpotifyToken $spotifyToken, bool $withScopeKey = false)
    {
        $cacheKey = $withScopeKey ? "spotify_token_{$spotifyToken->scope}" : 'spotify_token';
        return $this->setToUserCache($cacheKey, $spotifyToken, $spotifyToken->expires_in);
    }

    /**
     * Gets query for [[SpotifyToken]].
     *
     *
     * @return ActiveQuery
     */
    public function getSpotifyTokens()
    {
        return $this->hasMany(SpotifyToken::class, ['user_id' => 'id']);
    }
}
