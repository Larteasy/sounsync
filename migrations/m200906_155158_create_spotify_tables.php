<?php

use yii\db\Migration;

/**
 * Handles the creation of spotify tables.
 */
class m200906_155158_create_spotify_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%spotify_tokens}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'access_token' => $this->string()->notNull(),
            'token_type' => $this->string(),
            'scope' => $this->string(),
            'expires_in' => $this->integer(),
            'refresh_token' => $this->string(),
            'created_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk-spotify_tokens-user_id',
            '{{%spotify_tokens}}',
            '{{user_id}}',
            '{{%users}}',
            '{{id}}'
        );

        $this->createIndex(
            'index-spotify_tokens-user_id',
            '{{%spotify_tokens}}',
            '[[user_id]]'
        );

        $this->createTable('{{%spotify_tokens_history}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'access_token' => $this->string()->notNull(),
            'token_type' => $this->string(),
            'scope' => $this->string(),
            'refresh_token' => $this->string(),
            'created_at' => $this->dateTime(),
            'inactivated_at' => $this->dateTime(),
        ]);

        $this->addForeignKey(
            'fk-spotify_tokens_history-user_id',
            '{{%spotify_tokens_history}}',
            '{{user_id}}',
            '{{%users}}',
            '{{id}}'
        );

        $this->createIndex(
            'index-spotify_tokens_history-user_id',
            '{{%spotify_tokens_history}}',
            '[[user_id]]'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('index-spotify_tokens_history-user_id', '{{%spotify_tokens_history}}');
        $this->dropForeignKey('fk-spotify_tokens_history-user_id', 'spotify_tokens_history');
        $this->dropTable('{{%spotify_tokens_history}}');
        $this->dropIndex('index-spotify_tokens-user_id', '{{%spotify_tokens}}');
        $this->dropForeignKey('fk-spotify_tokens-user_id', 'spotify_tokens');
        $this->dropTable('{{%spotify_tokens}}');
    }
}
