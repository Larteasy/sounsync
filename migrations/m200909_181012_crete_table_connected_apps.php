<?php

use yii\db\Migration;

/**
 * Class m200909_181012_crete_table_connected_apps
 */
class m200909_181012_crete_table_connected_apps extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%connected_apps}}',
            [
                'id' => $this->primaryKey(),
                'name' => $this->string()->unique()->notNull(),
                'active' => $this->boolean()->notNull(),
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%connected_apps}}');
    }
}
