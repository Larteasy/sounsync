<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "connected_apps".
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 *
 * @property AppSetting[] $appSettings
 */
class ConnectedApp extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'connected_apps';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'active'], 'required'],
            [['active'], 'boolean'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * Gets query for [[AppSettings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAppSettings()
    {
        return $this->hasMany(AppSetting::className(), ['app_id' => 'id']);
    }
}
