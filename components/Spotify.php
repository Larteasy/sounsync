<?php

namespace app\components;

use app\models\AppSetting;
use yii\base\Component;
use Yii;

/**
 * Класс-компонент singleton для статических данных Spotify
 *
 * @property string $appName Получает имя приложения. ReadOnly
 * @property string $appID Получает ID приложения. ReadOnly
 * @property string $client_id client_id
 * @property string $client_secret client_secret
 */
class Spotify extends Component
{
    private $appName = 'Spotify';

    private $appID = 1;

    public function getAppId()
    {
        return $this->appID = 1;
    }

    public function getAppName()
    {
        return $this->appName;
    }

    public function getClient_id()
    {
        return Yii::$app->cache->getOrSet('spotify_client_id', function () {
            return AppSetting::getAppSetting($this->appName, 'client_id');
        });
    }

    public function getClient_secret()
    {
        return Yii::$app->cache->getOrSet('spotify_client_secret', function () {
            return AppSetting::getAppSetting($this->appName, 'client_secret');
        });
    }
}
