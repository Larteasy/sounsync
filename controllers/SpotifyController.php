<?php

namespace app\controllers;

use app\components\utils\AdvancedStringHelper;
use app\models\SpotifyToken;
use yii\base\InvalidConfigException;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\httpclient\Exception;
use yii\web\Controller;
use Yii;

class SpotifyController extends Controller
{
    private function getCacheKey(): string
    {
        return AdvancedStringHelper::makeUpCacheKey([Yii::$app->user->id, Yii::$app->spotify->appName, 'state']);
    }

    /**
     * @return \yii\web\Response
     * @throws \Throwable
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function actionStartAuthSpotifyClient()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }

        if ($spotifyToken = Yii::$app->user->identity->getActiveSpotifyToken()) {
            $spotifyToken->delete();
        }

        $scopes = 'user-read-private user-read-email';
        $redirectUri = 'http://localhost/spotify/save-spotify-client';
        $state = Yii::$app->getSecurity()->generateRandomString();
        $urlParams = http_build_query(
            [
                'response_type' => 'code',
                'client_id' => Yii::$app->spotify->client_id,
                'scope' => $scopes,
                'redirect_uri' => $redirectUri,
                'state' => $state
            ]
        );

        Yii::$app->user->identity->setToUserCache('spotify_state', $state);

        return $this->redirect('https://accounts.spotify.com/authorize?' . $urlParams);
    }

    /**
     * Получает токен доступа Spotify для пользователя
     *
     * @param string|null $code
     * @param string|null $state
     * @param string|null $error
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function actionSaveSpotifyClient ($code = null, $state = null, $error = null)
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect('/site/login');
        }

        if ($code) {
            if ($state === Yii::$app->user->identity->popFromUserCache('spotify_state')) {
                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('POST')
                    ->setUrl('https://accounts.spotify.com/api/token')
                    ->setData([
                        'grant_type' => 'authorization_code',
                        'code' => $code,
                        'redirect_uri' => 'http://localhost/spotify/save-spotify-client',
                        'client_id' => Yii::$app->spotify->client_id,
                        'client_secret' => Yii::$app->spotify->client_secret,
                    ])
                    ->send();

                if ($response->isOk) {
                    $spotifyToken = new SpotifyToken();
                    $spotifyToken->load($response->data, '');
                    $spotifyToken->user_id = Yii::$app->user->id;

                    if (!$spotifyToken->save()) {
                        $error = $spotifyToken->errors;
                        return $this->redirect(Url::to(['spotify/error', 'error' => $error]));
                    }

                    Yii::$app->user->identity->activeSpotifyToken = $spotifyToken;
                    return $this->redirect(Url::to(['/profile']));
                }
            } else {
                return $this->redirect(Url::to(['spotify/error', 'error' => 'wrong_state']));
            }
        }

        return $this->redirect(Url::to(['spotify/error', 'error' => $error]));
    }

    public function actionError($error = null)
    {
        return $this->render('error', ['error' => $error]);
    }
}