<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <p>
            <?= Html::a(
                Yii::t('app','Connect to Spotify'),
                'spotify/start-auth-spotify-client',
                ['class' => 'btn btn-success']
            ) ?>
        </p>
    </div>

    <div class="body-content">


    </div>
</div>
