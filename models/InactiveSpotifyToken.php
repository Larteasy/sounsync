<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "spotify_tokens_history".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property string|null $token_type
 * @property string|null $scope
 * @property string|null $refresh_token
 * @property string|null $created_at
 * @property string|null $inactivated_at
 *
 * @property User $user
 */
class InactiveSpotifyToken extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spotify_tokens_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'access_token'], 'required'],
            [['user_id'], 'default', 'value' => null],
            [['user_id'], 'integer'],
            [['created_at', 'inactivated_at'], 'safe'],
            [['access_token', 'token_type', 'scope', 'refresh_token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className() => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['inactivated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'token_type' => Yii::t('app', 'Token Type'),
            'scope' => Yii::t('app', 'Scope'),
            'refresh_token' => Yii::t('app', 'Refresh Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'inactivated_at' => Yii::t('app', 'Inactivated At'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
