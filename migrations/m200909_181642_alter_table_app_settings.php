<?php

use yii\db\Migration;

/**
 * Class m200909_181642_alter_table_app_settings
 */
class m200909_181642_alter_table_app_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropPrimaryKey(
            'pk-app_settings-app-setting',
            '{{%app_settings}}'
        );
        $this->addColumn(
            '{{%app_settings}}',
            'id',
            $this->integer()
        );
        $this->addPrimaryKey(
            'pk-app_settings-id',
            '{{%app_settings}}',
            'id'
        );
        $this->addColumn(
            '{{%app_settings}}',
            'app_id',
            $this->integer()
        );
        $this->addForeignKey(
            'fk-app_settings-app_id',
            '{{%app_settings}}',
            'app_id',
            '{{%connected_apps}}',
            'id'
        );
        $this->dropColumn(
            '{{%app_settings}}',
            'app'
        );
        $this->createIndex(
            'index-app_settings-app_id-setting',
            '{{%app_settings}}',
            ['app_id', 'setting'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn(
            '{{%app_settings}}',
            'app',
            $this->string()
        );
        $this->dropIndex(
            'index-app_settings-app_id-setting',
            '{{%app_setting}}'
        );
        $this->dropForeignKey(
            'fk-app_settings-app_id',
            '{{%app_settings}}'
        );
        $this->dropColumn(
            '{{%app_settings}}',
            'app_id'
        );
        $this->dropPrimaryKey(
            'pk-app_settings-id',
            'app_settings'
        );
        $this->dropColumn(
            '{{%app_settings}}',
            'id'
        );
        $this->addPrimaryKey(
            'pk-app_settings-app-setting',
            'app_settings',
            ['app', 'setting']
        );
    }
}
