<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "app_settings".
 *
 * @property string $setting
 * @property string|null $value
 * @property int $id
 * @property int|null $app_id
 *
 * @property ConnectedApp $app
 */
class AppSetting extends ActiveRecord
{
    /**
     * Получает настройку для приложения
     *
     * @param string $app Название приложения
     * @param string $setting Название настройки
     * @return mixed|null
     */
    public static function getAppSetting(string $app, string $setting)
    {
        $appSetting = AppSetting::find()
            ->innerJoinWith('app')
            ->where([
                '[[connected_apps.name]]' => $app,
                'setting' => $setting,
            ])->one();

        return $appSetting->value;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'app_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['setting', 'id'], 'required'],
            [['id', 'app_id'], 'default', 'value' => null],
            [['id', 'app_id'], 'integer'],
            [['setting', 'value'], 'string', 'max' => 255],
            [['app_id', 'setting'], 'unique', 'targetAttribute' => ['app_id', 'setting']],
            [['id'], 'unique'],
            [['app_id'], 'exist', 'skipOnError' => true, 'targetClass' => ConnectedApp::className(), 'targetAttribute' => ['app_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'setting' => Yii::t('app', 'Setting'),
            'value' => Yii::t('app', 'Value'),
            'id' => Yii::t('app', 'ID'),
            'app_id' => Yii::t('app', 'App ID'),
        ];
    }

    /**
     * Gets query for [[App]].
     *
     * @return ActiveQuery
     */
    public function getApp()
    {
        return $this->hasOne(ConnectedApp::className(), ['id' => 'app_id']);
    }
}
