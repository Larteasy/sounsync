<?php


namespace app\components\utils;

use yii\helpers\StringHelper;

class AdvancedStringHelper extends StringHelper
{
    public static function makeUpCacheKey(array $arrayOfStrings)
    {
        return implode('_', $arrayOfStrings);
    }
}