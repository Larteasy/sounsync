<?php

use yii\db\Migration;

/**
 * Class m200906_205732_add_columns_dates_and_status_to_user_table
 */
class m200906_205732_add_columns_dates_and_status_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%users}}', 'status', $this->boolean());
        $this->addColumn('{{%users}}', 'created_at', $this->dateTime());
        $this->addColumn('{{%users}}', 'updated_at', $this->dateTime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%users}}', 'status');
        $this->dropColumn('{{%users}}', 'created_at');
        $this->dropColumn('{{%users}}', 'updated_at');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200906_205732_add_columns_dates_and_status_to_user_table cannot be reverted.\n";

        return false;
    }
    */
}
