<?php

use yii\db\Migration;

/**
 * Class m200908_173559_alter_table_users_unique_username
 */
class m200908_173559_alter_table_users_unique_username extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%users}}', '[[username]]', $this->string()->unique()->notNull());
        $this->alterColumn('{{%users}}', '[[password]]', $this->string()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200908_173559_alter_table_users_unique_username cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200908_173559_alter_table_users_unique_username cannot be reverted.\n";

        return false;
    }
    */
}
