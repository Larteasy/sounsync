<?php

namespace app\models;

use app\components\behaviors\DeletedModelsHistory;
use app\components\utils\AdvancedStringHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "spotify_tokens".
 *
 * @property int $id
 * @property int $user_id
 * @property string $access_token
 * @property string|null $token_type
 * @property string|null $scope
 * @property int|null $expires_in
 * @property string|null $refresh_token
 * @property string|null $created_at
 *
 * @property User $user
 */
class SpotifyToken extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spotify_tokens';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'access_token'], 'required'],
            [['user_id', 'expires_in'], 'default', 'value' => null],
            [['user_id', 'expires_in'], 'integer'],
            [['access_token', 'token_type', 'scope', 'refresh_token'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className() => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            DeletedModelsHistory::class => [
                'class' => DeletedModelsHistory::class,
                'attributeMatching' => [
                    'expires_in' => NULL,
                ],
                'deletionDateAttribute' => 'inactivated_at',
                'historyModelClassName' => InactiveSpotifyToken::className(),
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'token_type' => Yii::t('app', 'Token Type'),
            'scope' => Yii::t('app', 'Scope'),
            'expires_in' => Yii::t('app', 'Expires In'),
            'refresh_token' => Yii::t('app', 'Refresh Token'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function delete()
    {
        if (parent::delete()) {
            Yii::$app->cache->delete(AdvancedStringHelper::makeUpCacheKey([$this->user_id, 'spotify_token']));
            return true;
        }

        return false;
    }
}
