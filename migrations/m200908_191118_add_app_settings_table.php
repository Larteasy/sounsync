<?php

use yii\db\Migration;

/**
 * Class m200908_191118_add_app_settings_table
 */
class m200908_191118_add_app_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('app_settings', [
            'app' => $this->string(),
            'setting' => $this->string(),
            'value' => $this->string(),
        ]);

        $this->addPrimaryKey('pk-app_settings-app-setting', 'app_settings', ['app', 'setting']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('app_settings');
    }
}
