<?php

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * Class DeletedModelsHistory
 * @package app\components\behaviors
 *

 */
class DeletedModelsHistory extends Behavior
{
    public $attributeMatching = [];

    public $deletionDateAttribute;

    public $deletionDateFormat;

    public $historyModelClassName = '';

    /** @var ActiveRecord */
    private $historyModel;

    private $config = [];

    /**
     * Получает массив конфига поведения для незаданных (внутри модели и конфигурационном файле приложения) настроек
     *
     * @return array
     */
    private function getDefaultBehaviorConfig(): array
    {
        return [
            'deletionDateAttribute' => 'deletion_date',
            'deletionDateFormat' => 'Y-m-d H:i:s',
        ];
    }

    /**
     * Загружает параметр поведения из конфига
     *
     * @param mixed $setting Параметр
     * @param string $settingNameInConfig Название параметра в конфиге
     * @return bool Была ли установлена переменная
     */
    private function loadSettingByAppConfig(&$setting, string $settingNameInConfig): bool
    {
        if (isset($setting)) {
            return true;
        }

        $setting = $this->config[$settingNameInConfig] ?? $setting;

        return isset($setting);
    }

    /**
     * Загружает параметры поведения из конфига
     *
     * @return $this
     */
    private function loadBehaviorConfig(): self
    {
        if ($this->config) {
            return $this;
        }
        $this->config = Yii::$app->params[self::className()] ?? [];

        if (!$this->config) {
            return $this;
        }

        $defaultConfigValues = $this->getDefaultBehaviorConfig();
        foreach ($defaultConfigValues as $setting => $defaultValue) {
            if (!$this->loadSettingByAppConfig($this->$setting, $setting)) {
                $this->$setting = $defaultValue;
            }
        }

        return $this;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'createHistoryRecord',
        ];
    }

    /**
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();
        $this->historyModel = Yii::createObject($this->historyModelClassName);
        $this->loadBehaviorConfig();
    }

    public function createHistoryRecord()
    {
        $this->setHistoryModelAttributes();

        if (!$this->historyModel->save()) {
            throw new Exception(json_encode($this->historyModel->errors));
        }
        return $this;
    }

    private function setHistoryModelAttributes()
    {
        $ownerAttributes = array_diff_key($this->owner->getAttributes(), $this->getExcludedAttributes());

        foreach ($ownerAttributes as $ownerAttributeName => $ownerAttributeValue) {
            $historyAttributeName = $this->attributeMatching[$ownerAttributeName] ?? $ownerAttributeName;
            $this->historyModel->$historyAttributeName = $ownerAttributeValue;
        }

        $deletionDateAttribute = $this->deletionDateAttribute;
        $this->historyModel->$deletionDateAttribute = date($this->deletionDateFormat);
    }

    private function getExcludedAttributes(): array
    {
        $excludedAttributes = [];

        foreach ($this->attributeMatching as $ownerAttributeName => $historyModelAttributeName) {
            if (!$historyModelAttributeName) {
                $excludedAttributes[$ownerAttributeName] = $ownerAttributeName;
            }
        }

        return $excludedAttributes;
    }
}